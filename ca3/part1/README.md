# Class Assignment 3 Report

# Part 1 :

1.Analysis
------------

Run the applications of the projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.

2.Setup
----------

Here I describe the initial setup before going to implementation.

1. Download VirtualBox and install.

2. Created a Virtual Machine(VM) called _mldevops_ and set the **IP** of the second network adapter as **192.168.56.10** by editing the file _01-netcfg.yaml_ as described in the class documentation
   
![img](https://i.imgur.com/LXh31Gz.png)

3.Jobs description
------------

A. Build and execute the **_gradle_basic_demo_** project (from the previous class assignments)

B. Build and execute the **_tut-basic-gradle_ project_** (from the previous assignments)

4.Implementation
----------
Each task steps is described below.

 * Setup: clone my personal DevOps repository into VM

![img](https://i.imgur.com/SgyXRxM.png)

- For this class assignment I need to go (inside the VM) into **_ca2/part2_** folder of my repository and use _tut-basic_ and _gradleapp_.

![img](https://i.imgur.com/d55cpBG.png)

![img](https://i.imgur.com/xUjaaCH.png)

### Job A:

From the previous class assignment this gradle project has 3 tasks:

![img](https://i.imgur.com/q6N8hvp.png)

 1. Execute the server, by executing the command `gradle executeServer`

 2. Backup sources of the application, by executing the command `gradle makeBackup` 

3. Make zip file of the sources of the application, by executing the command `gradle makeZip`

##### Task 1:

1. Build the _gradle_basic_demo_ project in the Virtual Machine (VM) running the command ``` ./gradlew build``` inside the project folder

   ![img](https://i.imgur.com/vMkMjtv.png)

* Problem: 
  - Build not successfully at first attempt. Why ??? Checking on file read/write permissions and see that _gradlew_ has only _read_ and _write_ permissions, not _execution_ permissons.

   ![img](https://i.imgur.com/0uRjI8T.png)  

* Solution: 
  - Grant _execution_ permission to _gradlew_ command  with `chmod +x gradlew` and build the project

   ![img](https://i.imgur.com/uhxM2aa.png)

2. Run the application in the VM

   `./gradlew run`

   ![img](https://i.imgur.com/gRj8ppA.png)

3. Start the _server_ in the VM with command `gradle executeServer´

   ![img](https://i.imgur.com/HkHruwK.png)

4. Run client _Maria_ on the _host_ machine 

   a. Open one terminal on the _host_ machine and run a client. 

   ```gradle runClient```

   ![img](https://i.imgur.com/06g4Hxq.png)

   b. Open another terminal on the _host_ machine and run another client _Tony_.

   ````gradle runClient```

   ![img](https://i.imgur.com/AMQ4LyL.png)

5. Check that on the VM the 2 clients are initiated

   ![img](https://i.imgur.com/MsGcLCS.png)

   ![img](https://i.imgur.com/uCY28TT.png)

   ![img](https://i.imgur.com/tinWUY6.png)


* The application is executed in the VM and is accessed in the _host_ machine via the VM IP address showing the values

##### Task 2:

1. Make the backup of the application sources and check that the backup was made

![img](https://i.imgur.com/zcYUIW0.png)

##### Task 3:

1. Make the zip file of the application sources and check that the zip file was made

![img](https://i.imgur.com/eJjLU2b.png)
______________________

### Job B:

From the previous class assignment, this application has 

1. Build and execute the _tut-basic-gradle_ demo application (from the previous class assignment)  (_gradle-basic-demo_) in the Virtual Machine (VM) using the command _./gradlew build_

![img](https://i.imgur.com/OKXdSQl.png)

* Problem:
    - The application is not running. The _gradlew_ file has no execution permission.

* Solution:
    - Grant execution permission to the file with `chmod +x gradlew`

![img](https://i.imgur.com/8XKuStz.png)

2 . Try to _build_ the application again

![img](https://i.imgur.com/3FTZ29L.png)

* Build successful on the VM

3. Start the Spring boot with the command ´./gradlew bootRun´

![img](https://i.imgur.com/F60zjML.png)

3. Check if I can access the application results on the _host_ machine by going to ´192.168.56.10:8080' in the browser

![img](https://i.imgur.com/L08Q4kO.png)


5.Conclusion
----------

* After this class assignment part1 I´m now able to start a virtual machine using _VirtualBox_ virtualization technology.
  
6.Finish the class assignment
-------

* Add tag `ca3-part1` to the repository