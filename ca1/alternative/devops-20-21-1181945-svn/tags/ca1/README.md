## Class Assignment 1 Report

The source code for this assignment is located in the folder [ca1/app]
(https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/ca1/app/)

## 1. Analysis

This class assignment is to implement a simple scenario illustrating a git workflow, keep track of it, and the usage in another version control system.

1. Add a new email field to the application

    - Accept only Employees with a valid email (e.g., an email must have the "@" sign)  
    - Don´t accept invalid email strings (no @, one word only,...) and null or empty strings.
     
2. Keep track of stable versions and create new features using Git.
   
3. Implement the solution recurring to another Version Control System, other than Git.
     
## 2. Design

 - Backend: 
   
    For the email field and respective validation and testing, a _private String email_ will be added to the Employee Class.  
    This field will have is attributes validated, first only for no null/empty values, and after to match a specific format (must have the "@" sign) to guarantee that it corresponds to the requirements.
    The Employees are added to a ``` DatabaseLoader.java````that acts as a Employee repository 
    When an invalid email is inserted, the application will display an **"invalid email" message.
       
 - Frontend:

    Will display Employee fields in a table, and the email field is added to it. To do this the _email_ is added in the ```app.js```, and the ```DatabaseLoader```, in order to be displayed in the browser.

## 3. Implementation

For this class assignment (ca1) is necessary to develop new features to an existing application. To do so, I describe below the workflow steps and actions made.
During the workflow, several commits were made using different branches. 
The main branch (master) received only stable versions of the Application, while the feature-specific branches received regular updates to the project (e.g. When an issue was closed, a commit was made to this branch).  
Afterwards, the stable version was merged with master and then pushed to the master remote repository.

## 3.1 Workflow/Tutorial
For this class assignment (ca1) is necessary to develop new features to an existing application. To do so, I describe below the workflow steps and actions made.

### Steps :

### 1. Setup of class assignment : 

    Repository was created in **BitBucket** and cloned into local machine in ```c:\resposml\devops\ ```


### 2. Bitbucket issues : were created on BitBucket to set open tasks to resolve. The issues that were open are [here] (https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/issues)

- Issues:

       #1 - Tag the initial version as v1.2.0. (master branch)
       #2 - Create a branch called email-field
       #3 - Add support for email field
       #4 - Add unit tests for testing the creation of Employees and the validation of their attributes (for instance, no null/empty values)
       #5 - Merge branch email-field with the master and create a new tag (e.g, v1.3.0)
       #6 - Create a branch called fix-invalid-email
       #7 - Add support to email format
       #8 - Add tag "v1.3.1" to master branch after fix-invalid-email branch merge
       #9 - Update final README file to complete class assignment

Issues are set to **resolve** state, by commit message or by the own BitBucket issues function.

### 3 . Under the Windows filesystem, I added the class assignments folders and the app
  
    - c:\reposml
        |
        |-- \devops-20-21-1181945
                              |
                              |-- \ca1
                              |     |
                              |     | -- \app (Application)
                              |
                              |-- \ca2
                              |-- \ca3
                              |-- \ca4
                              |-- \ca5
                              |-- \ca6

### 4. Add, commit and push this new folder structure
ìnside c:\resposml|devops-20-21-1181945 

``` $ git add . ```

``` $ git commit -m "class folders" ```

``` $ git push origin master```

## A. Initial version of the application

### 5 Tag the initial version (master branch) as v1.2.0 and check if tag was created
```git tag -a v1.2.0 -m "v1.2.0"```

### 6. Push tag to remote server
```git push origin v1.2.0 ``` **Issue #1 resolved**

## B. Develop new features
New features will be developed in branches after the master branch.

- Add email-field to application

  ### 7. Create a branch called **email-field**
  ```git branch email-field``` **Issue #2 resolved**
  ### 8. Move inside branch
  ```git checkout email-field```
  ### 9. Add support for email field
  Edit ```Employee.java``` and insert the following field in each section:

  - Attributes:
      
      add ```private String email;```
  
   - Constructor:
    ```
    public Employee(String firstName, String lastName, String description, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.email = email;
    }
    ```

  - Methods:
    ```
    public String getEmail() { return email; }
      
    public void setEmail(String email) { this.email = email; }
      
      @Override
      public String toString() {
          return "Employee{" +
              "id=" + id +
              ", firstName='" + firstName + '\'' +
              ", lastName='" + lastName + '\'' +
              ", description='" + description + '\'' +
              ", email='" + email + '\'' +
              '}';
      } 
    ```
  
  Edit ```DatabaseLoader.java``` and insert the following: 
  
  ``` 
      this.repository.save(new Employee("Frodo", "Baggins", "ring bearer", "frodo@mail.com"));
      this.repository.save(new Employee("Samwise", "Gamgee", "garderner", "sam@mail.com"));
      this.repository.save(new Employee("Legolas", "Greenleaf", "elf", "legolas@mail.com"));
      this.repository.save(new Employee("Meriadoc", "Brandybuck", "friend", "merry@mail.com"));
  ```
_With the above, 4 new Employees (with email-field) were added to the database._

  Edit ```app.js``` and insert the email field on:
   ```
      <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Description</th>
          <th>Email</th>
      </tr>
   ```
 ```
    <tr>
        <td>{this.props.employee.firstName}</td>
        <td>{this.props.employee.lastName}</td>
        <td>{this.props.employee.description}</td>
        <td>{this.props.employee.email}</td>
    </tr>
 ```
### 10. Commit changes made to add support for email and push into remote
  ```git add . ```

  ```git commit -m "add support for email , resolve #3"``` **Issue #3 resolved**

  ```git push --set-upstream origin email-field```

### 11. Add unit tests for testing the creation of Employees and the validation of their attributes

- Created a folder called ```tests``` and mark it as test sources root directory.
All the tests are inside the file ```EmployeeTest```.

- Create```EmployeeTest.java``` for testing the creation of Employees and the validation of their attributes (for instance, no null/empty values). Add the test to the repository.

``` git add . ```


``` git commit -m "add units tests, resolve #4" ``` **Issue #4 resolved**
   
### 12. Merge with master tagging v1.3.0
- Move from **email-field branch** to **master branch**  to do the merge.

  ```git checkout master```

- After moving to **master** branch, merge  **email-field** branch into master (fast-forward disabled) 

  ```git merge --no-ff email-field```  _(If conflicts are found, please fix and commit after)_

- Assign a new tag to this master version

  ```git tag -a v1.3.0 -m "v1.3.0``` **Issue #5 resolved**

## C. Fixing bugs
Bugs will be corrected in branches after the master branch. 
- Bug: Fix invalid email _(The server should only accept Employees
  with a valid email (e.g., an email must have the "@" sign)_
 
### 13. Create a new branch **fix-invalid-email** and move into with 
```git checkout -b fix-invalid-email``` **Issue #6 resolved**

### 14. Add support for bug fixing
  Edit ```Employee.java``` and update with the following validation to email field as a bug fixing feature to improve our implementation.

```
private boolean isAValidEmail(String email) {
		boolean valid;
		if (email == null || email.isEmpty() || email.trim().length() == 0) {
			valid = false;
		} else {
			String emailRegex = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
			Pattern pattern = Pattern.compile(emailRegex);
			valid = pattern.matcher(email).matches();
		}
		return valid;
	}
```

### 15. Add unit tests

Edit ```EmployeeTest.java``` and update with the following tests to add coverage for validation of email field as a bug fixing feature to improve our implementation.

```
@DisplayName("Success #1: Assert that email is int the correct format")
    @Test
    void shouldCreateEmployeeWithValidEmail() {
        String expected = "frodo@mail.com";
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer",  "frodo@mail.com");
        String result = employee.getEmail();
        assertEquals(expected, result);
    }

    @DisplayName("Fail #5: Assert that email is in the correct format")
    @Test
    void shouldReturnInvalidEmail() {
        String expected = INVALIDEMAIL;
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer",  "frodo@@mail.com");
        String result = employee.getEmail();
        assertEquals(expected, result);
    }
   
```

### 16. Commit the new unit tests for testing the creation of Employees and the validation of their attributes

``` git commit -m "add support for email, resolve #7" ``` **Issue #7 resolved**

### 17 Merge with master tagging v1.3.1
- Move from **fix-invalid-field branch** to **master branch**  to do the merge.

```git checkout master```

- After moving to **master** branch, merge  **fix-invalid-email** branch into master (fast-forward disabled)

```git merge --no-ff fix-invalid-email``` _(If conflicts are found, please fix and commit after)_

- Assign a new tag to this master version

```git tag -a v1.3.1 -m "v1.3.1``` **Issue #8 resolved**

### 18 End of assignment, tag master branch "ca1"

``` git status```

``` git add .```

``` git commit -m "end of class assignment" ```

``` git tag -a ca1 -m "ca1```

``` git push origin ca1```

``` git push```

-----------------------------------------------------------------------------------------

## 3.2. Analysis of an Alternative

- Version Control Systems are divided in two main types:

A. Distributed Version Control Systems (DVCS), such as **GIT** or **Mercurial** , where every user has a local copy of the repository in addition to the central repo on the server side as shown is picture below.

B. Centralized Version Control Systems (CVCS) such as **Subversion (SVN)** or **CVS**, the repository is located at one place and provides access to many clients.

[![CVCS-vs-DVCS.png](https://i.postimg.cc/tCfff1zZ/CVCS-vs-DVCS.png)](https://postimg.cc/qzsj6vS4)

**I decided for [Subversion (SVN)](https://subversion.apache.org/) as a GIT alternative. I use [SourceForge](https://sourceforge.net/) as a compatible source repository.**

### 3.2.1 Key concepts:

SVN does not have the concept of local repository/remote repository, accordingly commit is directly reflected in the remote.

The central repository has all the data in predefined format i.e. a tree structure , this allows the user to see all the changes made to it any time. And also user can recover any previous version of that project code.

[![svn-layout.png](https://i.postimg.cc/XvZpRNGH/svn-layout.png)](https://postimg.cc/yJH19HTc)

**Note :** in SVN system , _trunk_ folder is the correspondent of GIT _master_ branch.

  1 .Branches :

- Subversion has no internal concept of a branch—it knows only how to make copies. branches are created as directories inside the server. Many developers dislike this directory structure. This is different from other version control systems, where branches are typically defined by adding extra-dimensional “labels” to collections of files. The location of your branch directory doesn't matter to Subversion. 
  Most teams follow a convention of putting all branches into a /branches directory, but it is a decision that is optional, and we can put the different branches were we want to.
  
  2 .Tags :


- A tag is just a “snapshot” of a project in time. In Subversion, this idea already seems to be everywhere. Each repository revision is exactly that—a snapshot of the filesystem after each commit.

Conclusion : There is no difference between branches and tags in Subversion. The only difference is in what the user then does with the directory. Branches are typically created, edited, and then merged back into the trunk. Alternatively, tags are created as a snapshot of the project at a point in time and then never changed.


### 3.2.2 Differences: 

| Aspect     | Git      | SVN |
| ----------- | -------- | ----------- |
| Type | Distributed      | Centralized |
| Server Architecture | Installed in a workstation and acts as a clent and a server      | Separate server and client       |
| Repositories | Developers spend time waiting to check out the full repository onto their computer. Every time a large file is changed and committed, Git repositories grow exponentially  | Only the working tree and the latest changes are checked out onto local machines. Checkouts take less time in SVN when there are a lot of changes to binary files       |
| Branching |  References to a certain commit. They are lightweight — yet powerful. You can create, delete, and change a branch at any time, without affecting the commits | Are created as directories inside a repository. This directory structure is the core pain point with SVN branching. When the branch is ready, you commit back to the trunk.|
| Access controls| All the contributors have the same permissions | Specific read and write access controls per file level and per directory level.|
| History track | Mutable history. Changes are tracked at a repository level. The complete history of the repository is “backed up” each time a developer clones it to their computer | Immutable history. To make any change to the repository’s history, you need access to the central server. Changes are tracked at the file level.|
| Storage requirements | Same. Can´t handle large binary files | Same. Can handle large binary files |
| Usability | Uses the command line as the primary user interface. But the syntax in Git can overwhelm beginners | Uses the command line as the primary user interface. It is more readily used by non-programmers who want to version non-code assets |

**TL;DR** : **SVN Is Better For Performance**. When it comes to Git vs. SVN performance, the client-server model of SVN outperforms with larger files and codebases.

**TL;DR** : **Git Is Better For Branching** .Developers prefer Git because of its effective branching model.

**TL;DR** : **Tie** . Depending on your needs, either Git or SVN could be a better choice. Both systems take different approaches when it comes to permissions and access

**TL;DR** : **Tie** . Making regular backups is highly encouraged with both solutions, so any system is good.

**TL;DR** : **SVN Is Better For Binaries** . SVN is better at storing binary files. Storing large binary files in SVN would take up less space than in Git.

**TL;DR** : **SVN is Easier to Learn** .SVN often considered easier to learn. This is especially true for non-technical users. They are able to catch on to common operations quickly.

### 3.2.3. Implementation

### 3.2.3.1 Workflow/Tutorial
Here I present the steps made to develop new features to an existing application.To do so, I describe below the workflow steps and actions made.

### Steps :

### 1. Setup of class assignment :

 - Created my remote project repository in _SourceForge_ . 
    Repository can be found [here](https://sourceforge.net/p/devops-20-21-1181945/svn/HEAD/tree/).

### 2 . Cloned the remote repository into ```c:\reposml\devops\git-alternative``` using :

[![svn-1.jpg](https://i.postimg.cc/HscP3b9D/svn-1.jpg)](https://postimg.cc/WhVn1FyX)

**revision #0**
### 3. Moved into local repository folder and created project folders

[![svn-2.jpg](https://i.postimg.cc/qMqgFvNd/svn-2.jpg)](https://postimg.cc/ykqVgBhf)

### 4. Copy the original application folder to _trunk folder_ , rename folder to _app_ and add local project repository folders (trunk, branches and tags) to the working copy*.
*Ordinary directory tree on your local system, containing a collection of files. They will be uploaded and added to the repository on your next commit.

[![svn-3.jpg](https://i.postimg.cc/437S2Vgh/svn-3.jpg)](https://postimg.cc/v1yhc1DQ)

### 5. Commit the new folder tree structure with message "Add project directories and application" (Send changes from my working copy to the remote repository)

[![svn-4.jpg](https://i.postimg.cc/9fHm0VC4/svn-4.jpg)](https://postimg.cc/4Ywk2DKZ)

**revision #1** This is similar to Git _push_ command.

## A. Initial version of the application

### 6. Tag the "master" branch (where the stable version is) with _v1.2.0 .
Since SVN does not have tag concept as GIT, a _tag_ in SVN is just a copy of a project in a specific moment. So to have this:

- I moved into ```c:\reposml\devops\ca1-alt\devops-20-21-1181945-svn\tags``` and created the folder **_v1.2.0_**.

[![svn-5.jpg](https://i.postimg.cc/QtFQ0pKK/svn-5.jpg)](https://postimg.cc/k6dR5Rc7)

- Add folder ```c:\reposml\devops\ca1-alt\devops-20-21-1181945-svn\tags\v1.2.0``` to be committed after .
  
[![svn-6.jpg](https://i.postimg.cc/WbFZFgj1/svn-6.jpg)](https://postimg.cc/XpWqtZpR)

- Commit the new tag to the remote repository

[![svn-7.jpg](https://i.postimg.cc/XY2Xsvnz/svn-7.jpg)](https://postimg.cc/rDr8mM01)

 **revision #2**

- Added the working copy by copying it into the folder ```c:\reposml\devops\devops-20-21-1181945-svn\tags\v1.2.0``` and to be commited after

[![svn-7b.jpg](https://i.postimg.cc/4xkdFXNQ/svn-7b.jpg)](https://postimg.cc/gnsmxFhx)

**revision #3**

## B. Develop new features
New features will be developed in different branches .

- Add email-field to application

### 7. Create a branch called **email-field** in the remote repository and commit

[![svn-9c.jpg](https://i.postimg.cc/qqMdtZLY/svn-9c.jpg)](https://postimg.cc/bDM5Wg4H)

**revision #4**

### 8. Add support for email-field 

- First copy the remote repository branch email-field into local

[![svn-10a.jpg](https://i.postimg.cc/rwD151FY/svn-10a.jpg)](https://postimg.cc/cvNtS83Q)

[![svn-10b.jpg](https://i.postimg.cc/ncR5HGPd/svn-10b.jpg)](https://postimg.cc/ykRnjcB9)

- Same as used in GIT workflow, updated ``` Employee.java```, ```DatabaseLoader.java``` and ```app.js```

### 9. Commit changes made

[![svn-11.jpg](https://i.postimg.cc/Njq5NS6T/svn-11.jpg)](https://postimg.cc/0bnkjtH2)

**Note :** By mistake I made the commit without a commit message and closed bash command line and loose the picture to show this step. But the changes were commited and the remote server is up to date.

**revision #5**

### 10. Add unit tests for testing the creation of Employees and the validation of their attributes

- Same procedure as in GIT , created a folder _test_ and created a file ```EmployeeTest.java``` to validate attributes.

- Add files to SVN control and commit

[![svn-12.jpg](https://i.postimg.cc/3N894hFH/svn-12.jpg)](https://postimg.cc/DJN1HtNC)

**revision #6**

### 11. Merge with trunk and after add a new tag "v1.3.0" to project

- Find the revision _email-field_ local branch began with:
  
[![svn-13b.jpg](https://i.postimg.cc/FRvn28fq/svn-13b.jpg)](https://postimg.cc/kVjcSjvF)

This should display back to you the changes that have been made back to the point the branch was cut. Remember that number (should be rXXXX, where XXXX is the revision number)

- Move from **email-field branch** to **trunk branch**  and check out the updated revision number.

[![svn-13.jpg](https://i.postimg.cc/6pJW86FG/svn-13.jpg)](https://postimg.cc/RNd58mSM)

- Merge **email-field** branch into trunk

[![svn-14.jpg](https://i.postimg.cc/5yNKtzSS/svn-14.jpg)](https://postimg.cc/S23fTJ5n)

 - Commit the merge

[![svn-15.jpg](https://i.postimg.cc/Dwzzkf4V/svn-15.jpg)](https://postimg.cc/N5WtRcxb)

**revision #7**

- Add a new tag _v1.3.0 to the project in the remote repository

A copy of the trunk is made into ``` .../tags/v1.3.0 ``` (remote repository operations)

[![svn-16.jpg](https://i.postimg.cc/26TZFcPh/svn-16.jpg)](https://postimg.cc/1nnXyHR3)

- Check out a copy of the new tag v1.3.0 version of the project

[![svn-17.jpg](https://i.postimg.cc/7L5sRR31/svn-17.jpg)](https://postimg.cc/mcGyz86h)

**revision #8**

## C. Fixing bugs
Bugs will be corrected in separated branches.
- Bug: Fix invalid email _(The server should only accept Employees
  with a valid email (e.g., an email must have the "@" sign)_

### 12. Create a new branch **fix-invalid-email** 

- Create in remote repository

[![svn-18.jpg](https://i.postimg.cc/HswbjwmC/svn-18.jpg)](https://postimg.cc/nCLXPQMd)

- Copy into local

[![svn-19.jpg](https://i.postimg.cc/xCPScp6z/svn-19.jpg)](https://postimg.cc/sQ1LLmdj)

**revision #9**

### 13. Add support for bug fixing and commit

- Same as used in GIT workflow, edit ```Employee.java``` and update with the following validation to email field as a bug fixing feature to improve our implementation.

[![svn-20.jpg](https://i.postimg.cc/Qxh8xmzr/svn-20.jpg)](https://postimg.cc/G9gwXFrM)

**revision #10**

### 14. Add unit tests and commit

- Same as used in GIT workflow, edit ```EmployeeTest.java``` and update with more tests to validate format

[![svn-21.jpg](https://i.postimg.cc/CMjMgXmN/svn-21.jpg)](https://postimg.cc/rzw2Gn60)

**revision #11**

### 15. Merge with trunk and after add a new tag "v1.3.1" to project

- Find the revision **fix-invalid-email** local branch began with:

[![svn-22.jpg](https://i.postimg.cc/GppMd4GC/svn-22.jpg)](https://postimg.cc/Hrf0ts6Z)

This should display back to you the changes that have been made back to the point the branch was cut. Remember that number (should be rXXXX, where XXXX is the revision number)

- Move from **fix-invalid-email** to **trunk branch**  and check out the updated revision number.

[![svn-23.jpg](https://i.postimg.cc/Hk62ZTKP/svn-23.jpg)](https://postimg.cc/rDtxz2qS)

- Merge ****fix-invalid-email** branch into trunk and commit

[![svn-24.jpg](https://i.postimg.cc/fyHy4gNZ/svn-24.jpg)](https://postimg.cc/hQdcTsv3)

- Commit the merge

[![svn-25.jpg](https://i.postimg.cc/PfKP8N8c/svn-25.jpg)](https://postimg.cc/JtD1frxc)

**revision #12**

- Add a new tag _v1.3.1_ to the project in the remote repository

A copy of the trunk is made into ``` .../tags/v1.3.1 ``` (remote repository operations)

[![svn-26.jpg](https://i.postimg.cc/SRY3qn0s/svn-26.jpg)](https://postimg.cc/JHmP54zV)

**revision #13**
- Check out a copy of the new tag v1.3.1 version of the project

[![svn-27.jpg](https://i.postimg.cc/7PNmG19k/svn-27.jpg)](https://postimg.cc/z3Vn4g4t)

### 16. End of assignment, tag master branch "ca1"

Edit ```README.md``` file and update with all the class assignment information and tag with **ca1**

- Add a new tag _ca1_ to the project in the remote repository

[![svn-28.jpg](https://i.postimg.cc/D0Dgv6Qk/svn-28.jpg)](https://postimg.cc/ctRYTQ5X)

**revision #14**

 - Check out a copy of the new tag **ca1** version of the project

[![svn-29.jpg](https://i.postimg.cc/xCqkg2HY/svn-29.jpg)](https://postimg.cc/KkSvvCJs)