package basic_demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChatServerAppTest {

    @Test
    public void serverPortNotNullSuccess() {
        int serverPort = 3003;
        ChatServer server = new ChatServer(serverPort);
        assertNotNull(server);
    }

}