# Class Assignment 5 Report

# Part 1 :

1.Analysis
------------

Using **Jenkins** create a simple pipeline

2.Setup
----------

Here I describe the initial setup before going to implementation.

1. Download _jenkins.war_ file from [here](https://www.jenkins.io/download/). I select the file for _Windows_.

2. Copy the jenkins.war_ file to the class assignment folder (i.e. .../ca5)

3.Install **Jenkins** with the command `java -jar jenkins.war`

![jenkinsinstall](https://i.imgur.com/vKyOXsu.png)

![jenkinsinstall2](https://i.imgur.com/4UxWKQM.png)

*Note: _Jenkins_ is installed. Please open http://localhost:8080 to access Jenkins and execute any necessary additional setup step.

3.Tasks description
------------

1. **Checkout** : to checkout the code form the repository

   
2. **Assemble** : compiles and produces the archive files with the application.

- _Note_ : do not use the build task of gradle (because it also executes the tests)!
   
3. **Test** : executes the Unit Tests and publish in Jenkins the Test results. 
   
-  _Note_ : see the junit step for further information on how to archive/publish test results.
            Do not forget to add some unit tests to the project (maybe you already have done it).
   
4. **Archive** : archives in Jenkins the archive files (generated during Assemble)

4.Implementation
----------

Define pipeline as code (in a language called Groovy, the same used by Gradle).

By convention the script of the pipeline is stored on files named _**Jenkinsfile**_.

This file is usually located in the root folder of the source code repository.

4.1 . Create pipeline 
------------------------

1. Pipeline configuration:

Go to **Configure_ and define the repository and set the path for the _Jenkinsfile_.

-Note: the path if the _Jenkinsfile_ is relative to the root of the repository, in my case is ```https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/```, so in my case is on folder ```ca2/part1/gradle_basic_demo```.

![img](https://i.imgur.com/7Vkyo6S.png)

-Note: since my repository is _private_ a credential must be supplied to have access.

![img](https://i.imgur.com/7QSoMBC.png)

Create a very simple pipeline using the "gradle_basic_demo" application to have the following stages in my _Jenkinsfile_: 

1. **Checkout**: to checkout the code form the repository

    - Edit the _Jenkinsfile_ to have the following:
    
    ```
    stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'miguelloureiro-bitbucket-credentials', url: 'https://bitbucket.org/MiguelLoureiro1181945/devops-20-21-1181945/src/master/ca2/part1/gradle_basic_demo/'

            }
        }
    ```

2. **Assemble**: compiles and produces the archive files with the application. 
   Note: do not use the build task of gradle (because it also executes the tests)!

- Edit the _Jenkinsfile_ to have the following:

    ```
    stage('Assemble') {
                    steps {
                        echo 'Assembling...'
                        script {
                           if (isUnix())
                           dir ('ca2/part1/gradle_basic_demo') {
                           sh './gradle assemble'
                           }

                           else
                           dir ('ca2/part1/gradle_basic_demo'){
                           bat 'gradle assemble'
                           }

                        }
                    }
        }
    ```
  

-   _Note_ : the condition  ( if ... else ) is to allow the execution on _Unix (Mac laptops)_ or _Windows_ laptops. 

3. **Archive**. archives in Jenkins the archive files (generated during Assemble)

    ```
    stage('Archive') {
                    steps {
                        echo 'Archiving...'
                        archiveArtifacts 'ca2/part1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar'
                    }
                }
    ```


4. **Test**:. Executes the Unit Tests and publish in Jenkins the Test results. 
   Note: see the _junit_ step for further information on how to archive/publish test results.
   
    Note: do not forget to add some unit tests to the project (maybe you already have done it).
   
- Edit the _Jenkinsfile_ to have the following:

```
    stage ('Test'){
            steps {
                echo 'Testing...'
                script {
                    if (isUnix()) {
                    dir ('ca2/part1/gradle_basic_demo') {
                    sh './gradle clean test'
                    junit '**/TEST-basic_demo.AppTest.xml'
                    }

                    }

                    else {
                    dir ('ca2/part1/gradle_basic_demo') {
                    bat 'gradle clean test'
                    junit '**/TEST-basic_demo.AppTest.xml'
                    }

                    }

                    }
            }

        }

```

4.2 Execute pipeline
--------------

1. Build #1

   ![img](https://i.imgur.com/GP5NINg.png)

   ![img](https://i.imgur.com/uN0nvqk.png)    

- Note : I made a mistake and have only one unit test. Must add more unit tests.

- Add 2 more tests (ChatClientAppTest and ChatServerAppTest ) to the project as follows:

  1. ChatClientAppTest

    ```
        public class ChatClientAppTest {

            @Test
            public void chatClientAllArgsSuccess() {
            ChatClient chatClient = new ChatClient("balelas", 3003);
            assertNotNull(chatClient);
            }
        }

    ```

  2. ChatServerAppTest

    ```
        public class ChatServerAppTest {

            @Test
            public void serverPortNotNullSuccess() {
            int serverPort = 3003;
            ChatServer server = new ChatServer(serverPort);
            assertNotNull(server);
            }
        }

    ```

- Edit the _Jenkinsfile_ to have the following:
```
    stage ('Test'){
            steps {
                echo 'Testing...'
                script {
                    if (isUnix()) {
                    dir ('ca2/part1/gradle_basic_demo') {
                    sh './gradle clean test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    else {
                    dir ('ca2/part1/gradle_basic_demo') {
                    bat 'gradle clean test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    }
            }

        }

```


2. Build #2

![img](https://i.imgur.com/isZzjKd.png)


- Note : I made a mistake and still have the test stage in last. Must change so _**Archive**_ is in last.

3. Build #3

Edit the _Jenkinsfile_ and swapped the _test_ stage with tha _archive_ stage , like this:

```
        stage ('Test'){
            steps {
                echo 'Testing...'
                script {
                    if (isUnix()) {
                    dir ('ca2/part1/gradle_basic_demo') {
                    sh './gradle clean test'
                    junit '**/TEST-basic_demo.AppTest.xml'
                    }

                    }

                    else {
                    dir ('ca2/part1/gradle_basic_demo') {
                    bat 'gradle clean test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    }
            }

        }

       stage('Archive') {
                           steps {
                               echo 'Archiving...'
                               archiveArtifacts 'ca2/part1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar'
                           }
               }


    }
}

```

- Note: build#3 failed !!!

![img](https://i.imgur.com/giBkz35.png)

4. Build #4

Edited the _Jenkinsfile_ and removed the path for the build artifacts.
Tried again, but the build failed!

![img](https://i.imgur.com/EAxe0iB.png)

5. Build #5

Edited the _Jenkinsfile_ and changed the following:

```
stage ('Test'){
            steps {
                echo 'Testing...'
                script {
                    if (isUnix()) {
                    dir ('ca2/part1/gradle_basic_demo') {
                    sh './gradle test'
                    junit '**/TEST-basic_demo.AppTest.xml'
                    }

                    }

                    else {
                    dir ('ca2/part1/gradle_basic_demo') {
                    bat 'gradle test'
                    junit '**/TEST-*.xml'
                    }

                    }

                    }
            }

        }

       stage('Archive') {
                           steps {
                               echo 'Archiving...'
                               archiveArtifacts 'ca2/part1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar'
                           }
               }



}

```


![img](https://i.imgur.com/aqvzIDa.png)

- Note: build failed again! 
        Cause: typo error (maybe some { out of place)

6. Build #6

Edited the _Jenkinsfile_ to fix the _typo_ error.

But build failed again! 

![img](https://i.imgur.com/Y9ISjjp.png)

7. Build #7

Fixed _typo_ error definitely but build failed again.

![img](https://i.imgur.com/7wyYL1a.png)

- Reason: the previous successful build left artifacts, and they must be deleted before new build.


- Correction: edited the _Jenkinsfile_ to execute a _clean_ task before _assemble_ , like the following:

```
 stage('Assemble') {
                    steps {
                        echo 'Assembling...'
                        script {
                           if (isUnix())
                           dir ('ca2/part1/gradle_basic_demo') {
                           sh './gradle clean'
                           sh './gradle assemble'
                           }

                           else
                           dir ('ca2/part1/gradle_basic_demo'){
                           bat 'gradle clean'
                           bat 'gradle assemble'
                           }

                        }
                    }
        }

```


![img](https://i.imgur.com/Cv6QSzb.png)


![img](https://i.imgur.com/VzL48wg.png)

9. Build #9

Only to check it all was ok.

![img](https://i.imgur.com/oqVKHmC.png)

- Note: build successful.

5.Conclusion
----------

Learned how to create a _Jenkins_ pipeline with the creation of several jobs to automate a build.

6.Finish the class assignment
-------

* Add tag `ca5-part1` to the repository